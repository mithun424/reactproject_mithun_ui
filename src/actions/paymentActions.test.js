import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as TYPE from "./action-types";
import * as paymentActions from "./paymentAction";
import axios from "axios";

jest.mock("axios");

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("paymentActions", () => {
  it("fetchPaymentDetails success", () => {
    axios.get.mockImplementationOnce(() => Promise.resolve([]));

    const store = mockStore({ entities: [], loading: true, error: null });
    return store.dispatch(paymentActions.fetchPaymentDetails());
  });

  it("addPayemntDetail success", () => {
    axios.post.mockImplementationOnce(() => Promise.resolve(true));
    window.alert = jest.fn();
    const expectActions = [{ type: "ADD_PAYMENTDETAIL_SUCCESS" }];
    const store = mockStore({ entities: [], loading: true, error: null });
    return store.dispatch(paymentActions.addPaymentDetail()).then(() => {
      expect(store.getActions()).toEqual(expectActions);
    });
  });

  it("fetchPaymentDetails failure", () => {
    axios.get.mockImplementationOnce(() => Promise.reject());

    const store = mockStore({ entities: [], loading: true, error: null });
    return store.dispatch(paymentActions.fetchPaymentDetails());
  });

  it("should create an action to FETCH_PAYMENTDETAILS_BEGIN", () => {
    const expectedAction = {
      type: TYPE.FETCH_PAYMENTDETAILS_BEGIN,
    };
    expect(paymentActions.fetchPaymentDetailsBegin()).toEqual(expectedAction);
  });

  it("should create an action to FETCH_PAYMENTDETAILS_SUCCESS", () => {
    const payments = [
      {
        id: 1,
        paymentdate: "2020-11-10T09:48:47.736+00:00",
        type: "CR",
        amount: 10000,
        custId: 123,
      },
    ];
    const expectedAction = {
      type: TYPE.FETCH_PAYMENTDETAILS_SUCCESS,
      payload: payments,
    };
    expect(paymentActions.fetchPaymentDetailsSuccess(payments)).toEqual(
      expectedAction
    );
  });

  it("should create an action to FETCH_PAYMENTDETAILS_FAILURE", () => {
    const error = {
      message: "Failed to fetch payment.. please try again later",
    };
    const expectedAction = {
      type: TYPE.FETCH_PAYMENTDETAILS_FAILURE,
      payload: error,
    };
    expect(paymentActions.fetchPaymentDetailsFailure(error)).toEqual(
      expectedAction
    );
  });

  it("should create an action to ADD_PAYMENTDETAIL_BEGIN", () => {
    const expectedAction = {
      type: TYPE.ADD_PAYMENTDETAIL_BEGIN,
    };
    expect(paymentActions.addPayemntDetailBegin()).toEqual(expectedAction);
  });

  it("should create an action to ADD_PAYMENTDETAIL_SUCCESS", () => {
    const expectedAction = {
      type: TYPE.ADD_PAYMENTDETAIL_SUCCESS,
    };
    expect(paymentActions.addPayemntDetailsSuccess()).toEqual(expectedAction);
  });

  it("should create an action to ADD_PAYMENTDETAIL_FAILURE", () => {
    const error = {
      message: "Failed to add new payment detail.. please try again later",
    };
    const expectedAction = {
      type: TYPE.ADD_PAYMENTDETAIL_FAILURE,
      payload: error,
    };
    expect(paymentActions.addPayemntDetailFailure(error)).toEqual(
      expectedAction
    );
  });
});
