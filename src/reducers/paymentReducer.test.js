import * as TYPE from "./../actions/action-types";
import paymentReducer from "./paymentReducer";

describe("PaymentReducer", () => {
  it("should return initial state", () => {
    const result = paymentReducer(undefined, {});
    expect(result).toEqual({ entities: [], loading: true, error: null });
  });

  it("should return FETCH_PAYMENTDETAILS_BEGIN state", () => {
    const action = {
      type: TYPE.FETCH_PAYMENTDETAILS_BEGIN,
    };
    const result = paymentReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({ entities: [], loading: true, error: null });
  });

  it("should return ADD_PAYMENTDETAIL_BEGIN state", () => {
    const action = {
      type: TYPE.ADD_PAYMENTDETAIL_BEGIN,
    };
    const result = paymentReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({ entities: [], loading: true, error: null });
  });

  it("should return FETCH_PAYMENTDETAILS_SUCCESS state", () => {
    const action = {
      type: TYPE.FETCH_PAYMENTDETAILS_SUCCESS,
      payload: [
        {
          id: 1,
          paymentdate: "2020-11-10T09:48:47.736+00:00",
          type: "CR",
          amount: 10000,
          custId: 123,
        },
      ],
    };
    const result = paymentReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [
        {
          id: 1,
          paymentdate: "2020-11-10T09:48:47.736+00:00",
          type: "CR",
          amount: 10000,
          custId: 123,
        },
      ],
      loading: false,
      error: null,
    });
  });

  it("should return FETCH_PAYMENTDETAILS_FAILURE state", () => {
    const action = {
      type: TYPE.FETCH_PAYMENTDETAILS_FAILURE,
      payload: {
        message: "Failed to fetch payment.. please try again later",
      },
    };
    const result = paymentReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      loading: false,
      error: { message: "Failed to fetch payment.. please try again later" },
    });
  });

  it("should return ADD_PAYMENTDETAIL_FAILURE state", () => {
    const action = {
      type: TYPE.ADD_PAYMENTDETAIL_FAILURE,
      payload: {
        message: "Failed to add new payment detail.. please try again later",
      },
    };
    const result = paymentReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      loading: false,
      error: {
        message: "Failed to add new payment detail.. please try again later",
      },
    });
  });

  it("should return ADD_PAYMENTDETAIL_SUCCESS state", () => {
    const action = {
      type: TYPE.ADD_PAYMENTDETAIL_SUCCESS,
    };
    const result = paymentReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      loading: false,
      error: null,
    });
  });
});
