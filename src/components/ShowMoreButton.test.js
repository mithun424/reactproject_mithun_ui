import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import ShowMoreButton from "./ShowMoreButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("render ShowMoreButton component", () => {
  const toggle = jest.fn();
  let visible = true;
  act(() => {
    render(<ShowMoreButton visible={visible} toggle={toggle} />, container);
  });

  // get ahold of the button element, and trigger some clicks on it
  const btnShowMore = document.getElementById("btnShowMore");
  expect(btnShowMore.innerHTML).toBe("Hide");

  act(() => {
    btnShowMore.dispatchEvent(new MouseEvent("click", { visible: false }));
  });
  expect(toggle).not.toHaveBeenCalled();
  expect(btnShowMore.innerHTML).toBe("Hide");
});
