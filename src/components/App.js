import React, { useEffect, useState, useCallback } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./Navbar";
import CreatPaymentForm from "./CreatePaymentForm";
import PaymentList from "./PaymentList";
import Banner from "./Banner";
import { useDispatch } from "react-redux";
import { fetchPaymentDetails } from "../actions/paymentAction";

const App = () => {
  const dispatch = useDispatch();
  const [refresh, setRefresh] = useState(false);

  const initFetchPaymentDetails = useCallback(() => {
    dispatch(fetchPaymentDetails());
  }, [dispatch]);

  useEffect(() => {
    initFetchPaymentDetails();
  }, [refresh, initFetchPaymentDetails]);

  return (
    <Router>
      <main role="main">
        <Navbar />
        <br />
        <Switch>
          <Route exact path="/">
            <Banner />
            <PaymentList />
          </Route>
          <Route path="/add">
            <CreatPaymentForm refresh={refresh} setRefresh={setRefresh} />
          </Route>
        </Switch>
      </main>
    </Router>
  );
};

export default App;
