import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Payment from "./Payment";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("render Navbar component", () => {
  const id = 123;
  const custId = 12;
  const type = "DR";
  const amount = 100;
  const paymentdate = new Date().toLocaleString();
  act(() => {
    render(
      <Payment
        key={id}
        custId={custId}
        type={type}
        amount={amount}
        paymentdate={paymentdate}
      />,
      container
    );
  });
  expect(container.textContent).toMatch(/Customer ID/i);
});
