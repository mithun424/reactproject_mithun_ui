import React from "react";
import { connect } from "react-redux";
import { addPaymentDetail } from "./../actions/paymentAction";
import { withRouter } from "react-router-dom";
import { compose } from "redux";

class CreatPaymentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      custId: "",
      type: "",
      amount: "",
    };
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const success = await this.props.addPaymentDetail({
      id: Math.floor(Math.random() * Math.floor(9999)),
      custId: this.state.custId,
      type: this.state.type,
      amount: this.state.amount,
      paymentdate: new Date(),
    });

    if (success) this.props.setRefresh(!this.props.refresh);
    this.props.history.push("/");
  };

  render() {
    return (
      <div className="container">
        <h3> Add New Payment</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-row">
            <div className="form-group col-md-5">
              <label>Customer Id:</label>
              <input
                id="custId"
                className="form-control"
                type="number"
                value={this.state.custId}
                onChange={(e) => this.setState({ custId: e.target.value })}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-5">
              <label>Payment Type:</label>
              <input
                id="type"
                className="form-control"
                type="text"
                value={this.state.type}
                onChange={(e) => this.setState({ type: e.target.value })}
              />
            </div>
            <div className="form-group col-md-5">
              <label>Amount:</label>
              <input
                id="amount"
                className="form-control"
                type="number"
                value={this.state.amount}
                onChange={(e) => this.setState({ amount: e.target.value })}
              />
            </div>
          </div>
          <div>
            <input
              type="submit"
              className="btn btn-primary"
              value="Create Payment"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default compose(
  withRouter,
  connect(null, { addPaymentDetail })
)(CreatPaymentForm);
