import React from "react";
import Payment from "./Payment";
import { useSelector } from "react-redux";

const PaymentList = () => {
  const payments = useSelector((state) => state.payment.entities);
  const error = useSelector((state) => state.payment.error);
  const loading = useSelector((state) => state.payment.loading);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div className="album py-5">
      <div className="container">
        <div className="row">
          {payments.map((payment) => (
            <Payment
              key={payment.id}
              custId={payment.custId}
              type={payment.type}
              amount={payment.amount}
              paymentdate={payment.paymentdate}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default PaymentList;
