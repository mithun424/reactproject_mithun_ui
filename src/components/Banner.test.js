import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Banner from "./Banner";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("render Banner component", () => {
  act(() => {
    render(<Banner />, container);
  });
  expect(container.textContent).toMatch(/Payment example/i);
});
