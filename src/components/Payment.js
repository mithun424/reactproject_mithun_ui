import React, { useState } from "react";
import ShowMoreButton from "./ShowMoreButton";
import PaymentDetails from "./PaymentDetails";

const Payment = ({ custId, type, amount, paymentdate }) => {
  const [visible, setVisible] = useState(false);

  return (
    <div className="card col-md-4" style={{ width: "18rem" }}>
      <div className="card-body">
        <h5 className="card-title">
          <b>Customer ID: </b>
          {custId}
        </h5>
        <ShowMoreButton toggle={setVisible} visible={visible} />
        <PaymentDetails
          type={type}
          amount={amount}
          paymentdate={paymentdate}
          visible={visible}
        />
      </div>
    </div>
  );
};

export default Payment;
