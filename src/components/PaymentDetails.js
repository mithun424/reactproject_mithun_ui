import React from "react";

const PaymentDetails = (props) => {
  return (
    props.visible && (
      <>
        <p>
          <b>Payment Type: </b> {props.type}
        </p>
        <p>
          <b>Amount: </b>
          {props.amount}
        </p>
        <p>
          <b>Payment Date:</b> {new Date(props.paymentdate).toDateString()}
        </p>
      </>
    )
  );
};

export default PaymentDetails;
