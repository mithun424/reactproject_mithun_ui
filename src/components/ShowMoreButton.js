import React from "react";

const ShowMoreButton = (props) => (
  <button
    id="btnShowMore"
    className="btn btn-primary"
    onClick={() => props.toggle(!props.visible)}
  >
    {props.visible ? "Hide" : "Show More!"}
  </button>
);

export default ShowMoreButton;
