import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => (
  <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
    <div className="navbar-collapse">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <NavLink exact className="nav-link" to="/">
            Payment List
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink exact className="nav-link" to="/add">
            Add Payment
          </NavLink>
        </li>
      </ul>
    </div>
  </nav>
);

export default Navbar;
