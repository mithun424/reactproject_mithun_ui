import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import PaymentDetails from "./PaymentDetails";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("render PaymentDetails component", () => {
  const type = "DR";
  const amount = 100;
  const paymentdate = new Date().toLocaleString();
  act(() => {
    render(
      <PaymentDetails
        visible={true}
        type={type}
        amount={amount}
        paymentdate={paymentdate}
      />,
      container
    );
  });
  expect(container.textContent).toMatch(/Payment Type/i);

  act(() => {
    render(
      <PaymentDetails
        visible={false}
        type={type}
        amount={amount}
        paymentdate={paymentdate}
      />,
      container
    );
  });
  expect(container.textContent).not.toMatch(/Payment Type/i);
});
